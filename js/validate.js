var showMessage = function (id, message) {
  document.getElementById(id).style.display = "block";
  document.getElementById(id).innerHTML = message;
};

var checkDuplicate = function (inputAcc, staffList) {
  var viTri = staffList.findIndex(function (item) {
    return inputAcc == item.account;
  });
  if (viTri == -1) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", "The account is existing");
    return false;
  }
};

var checkBlank = function (id, value) {
  if (!value) {
    showMessage(id, "Please input the field");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
};

var checkAccount = function (id, value) {
  var integer = /^[0-9]{4,6}$/;

  if (integer.test(value)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Please enter 4-6 integers");
    return false;
  }
};

var checkName = function (id, staffName) {
  var alphaExp = /^[a-zA-Z]+$/;
  if (alphaExp.test(staffName)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Please enter only alphabets");
    return false;
  }
};

var checkEmail = function (id, staffEmail) {
  const re =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  // /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(staffEmail)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Invalid email");
    return false;
  }
};

var checkPassW = function (id, pw) {
  if (
    /[A-Z]/.test(pw) &&
    /[0-9]/.test(pw) &&
    /[.!#$%&'*+/=?^_`{|}~-]/.test(pw) &&
    pw.length >= 4 &&
    pw.length <= 6
  ) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(
      id,
      "Password must include at least 1 digit, 1 capital leter, 1 special symbol, 4-6 characters"
    );
    return false;
  }
};

var checkDate = function (id, date) {
  var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
  if (date_regex.test(date)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Date format is MM/DD/YYYY");
    return false;
  }
};

var checkBaseSal = function (id, staffSal) {
  if ((staffSal >= 10000000) & (staffSal <= 20000000)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Salary ranges from VND10mil to VND20 mil");
    return false;
  }
};

var checkPosition = function (id, staffPosition) {
  if (staffPosition == 0) {
    showMessage(id, "Please choose position");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
};

var checkHours = function (id, staffHours) {
  if ((staffHours >= 80) & (staffHours <= 200)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Hours range from 80 to 200");
    return false;
  }
};
