var staffList = [];

var dataJSON = localStorage.getItem("STAFF_LOCAL");
if (dataJSON != null) {
  var list = JSON.parse(dataJSON);
  for (var i = 0; i < list.length; i++) {
    var staff = new Staff(
      list[i].account,
      list[i].name,
      list[i].email,
      list[i].passW,
      list[i].datepicker,
      list[i].baseSal,
      list[i].position,
      list[i].selectedPosition,
      list[i].hours,
      list[i].type
    );
    staffList.push(staff);
  }
  renderLayout(staffList);
}

function addUser() {
  var staff = getInfoFromForm();

  var isValid = checkDuplicate(staff.account, staffList);

  isValid =
    isValid &&
    checkBlank("tbTKNV", staff.account) &
      checkAccount("tbTKNV", staff.account) &
      checkBlank("tbTen", staff.name) &
      checkName("tbTen", staff.name) &
      checkBlank("tbEmail", staff.email) &
      checkEmail("tbEmail", staff.email) &
      checkBlank("tbMatKhau", staff.passW) &
      checkPassW("tbMatKhau", staff.passW) &
      checkBlank("tbNgay", staff.datepicker) &
      checkDate("tbNgay", staff.datepicker) &
      checkBlank("tbLuongCB", staff.baseSal) &
      checkBaseSal("tbLuongCB", staff.baseSal) &
      checkPosition("tbChucVu", staff.position) &
      checkBlank("tbGiolam", staff.hours) &
      checkHours("tbGiolam", staff.hours);
  if (isValid) {
    staffList.push(staff);

    var dataJSON = JSON.stringify(staffList);
    localStorage.setItem("STAFF_LOCAL", dataJSON);

    renderLayout(staffList);
  }

  resetForm();
}

// delete

function deleteSt(id) {
  var viTri = staffList.findIndex(function (item) {
    return item.account == id;
  });
  staffList.splice(viTri, 1);
  var dataJSON = JSON.stringify(staffList);
  localStorage.setItem("STAFF_LOCAL", dataJSON);

  renderLayout(staffList);
}

// edit

function edit(id) {
  var viTri = staffList.findIndex(function (item) {
    return item.account == id;
  });
  document.getElementById("tknv").disabled = true;
  showStaffInfoToEdit(staffList[viTri]);
}

function updateUser() {
  var staff = getInfoFromForm();

  var isValid =
    checkBlank("tbTKNV", staff.account) &
    checkAccount("tbTKNV", staff.account) &
    checkBlank("tbTen", staff.name) &
    checkName("tbTen", staff.name) &
    checkBlank("tbEmail", staff.email) &
    checkEmail("tbEmail", staff.email) &
    checkBlank("tbMatKhau", staff.passW) &
    checkPassW("tbMatKhau", staff.passW) &
    checkBlank("tbNgay", staff.datepicker) &
    checkDate("tbNgay", staff.datepicker) &
    checkBlank("tbLuongCB", staff.baseSal) &
    checkBaseSal("tbLuongCB", staff.baseSal) &
    checkPosition("tbChucVu", staff.position) &
    checkBlank("tbGiolam", staff.hours) &
    checkHours("tbGiolam", staff.hours);

  var viTri = staffList.findIndex(function (item) {
    return item.account == staff.account;
  });

  if ((viTri !== -1) & (isValid == true)) {
    staffList[viTri] = staff;

    var dataJSON = JSON.stringify(staffList);
    localStorage.setItem("STAFF_LOCAL", dataJSON);

    renderLayout(staffList);
    document.getElementById("tknv").disabled = false;
  }

  resetForm();
}

// filter staff with the rank

function findStaff() {
  var searchedRank = document.getElementById("searchName").value;
  var newArr = staffList.filter(function (item) {
    return item.rank().toLowerCase() == searchedRank.toLowerCase();
  });

  renderLayout(newArr);
  if (searchedRank == "") {
    renderLayout(staffList);
  }
}

// reset

function resetForm() {
  document.getElementById("inputForm").reset();
}
