function Staff(
  _account,
  _name,
  _email,
  _passW,
  _datepicker,
  _baseSal,
  _position,
  _selectedPosition,
  _hours
  // _type
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.passW = _passW;
  this.datepicker = _datepicker;
  this.baseSal = _baseSal;
  this.position = _position;
  this.selectedPosition = _selectedPosition;
  this.hours = _hours;
  // this.type = _type;
  this.totalSal = function () {
    if (this.position == 1) {
      return this.baseSal * 3;
    } else if (this.position == 2) {
      return this.baseSal * 2;
    } else {
      return this.baseSal;
    }
  };
  this.rank = function () {
    if (this.hours >= 192) {
      return "Xuất sắc";
    } else if (this.hours >= 176) {
      return "Giỏi";
    } else if (this.hours >= 160) {
      return "Khá";
    } else {
      return "Trung Bình";
    }
  };
}
