function getInfoFromForm() {
  var account = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var passW = document.getElementById("password").value;
  var datepicker = document.getElementById("datepicker").value;
  var baseSal = document.getElementById("luongCB").value * 1;
  var position = document.getElementById("chucvu").value;
  var hours = document.getElementById("gioLam").value * 1;

  var e = document.getElementById("chucvu");
  var selectedPosition = e.options[e.selectedIndex].text;

  var staff = new Staff(
    account,
    name,
    email,
    passW,
    datepicker,
    baseSal,
    position,
    selectedPosition,
    hours
  );
  return staff;
}

function renderLayout(staffList) {
  var content = "";
  for (var i = 0; i < staffList.length; i++) {
    var contentTr = `<tr>
        <td>${staffList[i].account}</td>
        <td>${staffList[i].name}</td>
        <td>${staffList[i].email}</td>
        <td>${staffList[i].datepicker}</td>
        <td>${staffList[i].selectedPosition}</td>
        <td>${staffList[i].totalSal().toLocaleString()}</td>
        <td>${staffList[i].rank()}</td>
        <td>
        <button data-toggle="modal"
        data-target="#myModal" class="btn btn-success" onclick="edit(${
          staffList[i].account
        })">Edit</button>
        <button class="btn btn-danger" onclick="deleteSt(${
          staffList[i].account
        })">Delete</button>
        </td>

        </tr>`;
    content += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = content;
}

function showStaffInfoToEdit(st) {
  document.getElementById("tknv").value = st.account;
  document.getElementById("name").value = st.name;
  document.getElementById("email").value = st.email;
  document.getElementById("password").value = st.passW;
  document.getElementById("datepicker").value = st.datepicker;
  document.getElementById("luongCB").value = st.baseSal;
  document.getElementById("chucvu").value = st.position;
  document.getElementById("gioLam").value = st.hours;
  document.getElementById("searchName").value = st.type;
}
